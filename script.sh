#!/bin/bash

cat <<EOT >> /etc/sysconfig/watchlog
# Configuration file for my watchdog service
# Place it to /etc/sysconfig
# File and word in that file that we will be monit
WORD="test"
LOG="/var/log/watchlog.log"
EOT


cat <<EOT>> /var/log/watchlog.log
test
test
test
ALERT
test
test
ALERT
EOT

cat <<EOT>> /usr/bin/watchlog.sh
#!/bin/bash
WORD=\$1
LOG=\$2
DATE=\$(date +"%m-%d-%Y %k:%M:%S")
if grep \$WORD \$LOG &> /dev/null
then
logger "\$DATE I found word, Master!" 
else
exit 0
fi 
EOT

chmod +x /usr/bin/watchlog.sh

cat <<EOT>> /lib/systemd/system/watchlog.service
[Unit]
Description=My watchlog service
[Service]
Type=oneshot
EnvironmentFile=/etc/sysconfig/watchlog
ExecStart=/bin/bash /usr/bin/watchlog.sh \$WORD \$LOG

EOT

cat <<EOT>> /lib/systemd/system/watchlog.timer
[Unit]
Description=Run watchlog script every 30 second
[Timer]
# Run every 30 second
OnUnitActiveSec=30
Unit=watchlog.service
[Install]
WantedBy=multi-user.target
EOT

systemctl daemon-reload
systemctl start watchlog.timer
systemctl start watchlog.service





