#!/bin/bash


cat <<EOT> /usr/bin/script3.sh
> /var/testscript
for i in {0..3}
do
DATE=\$(date +"%m-%d-%Y %k:%M:%S")
echo \$DATE >> /var/testscript
echo \$DATE
sleep 10
done
exit 143
EOT

chmod +x /usr/bin/script3.sh

cat <<EOT> /lib/systemd/system/exit143.service 
[Unit]
Description=Otus exit143 service.

[Service]
Type=simple
ExecStart=/bin/bash /usr/bin/script3.sh
SuccessExitStatus=143
Restart=on-success
MemoryLimit=100M
CPUQuota=5%
CPUAccounting=1
TasksMax=10

[Install]
WantedBy=multi-user.target
EOT

systemctl daemon-reload
systemctl start exit143.service
