#!/bin/bash
yum install httpd -y
systemctl stop httpd
mv /usr/lib/systemd/system/httpd.service /usr/lib/systemd/system/httpd@.service
sed -i '/EnvironmentFile/s/httpd/httpd-%I/g' /usr/lib/systemd/system/httpd@.service
echo "OPTIONS=-f conf/first.conf" > /etc/sysconfig/httpd-first
echo "OPTIONS=-f conf/second.conf" > /etc/sysconfig/httpd-second
cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/first.conf 
cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/second.conf
sed -i '/Listen 80/s/80/8080/g' /etc/httpd/conf/second.conf
echo "PidFile /var/run/httpd-second.pid" >> /etc/httpd/conf/second.conf
systemctl daemon-reload
systemctl start httpd@first
systemctl start httpd@second
#sed -i '/SOCKET/s/^#\+//' /etc/sysconfig/spawn-fcgi


