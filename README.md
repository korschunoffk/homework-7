# Домашняя работа - SystemD

# 1. Работа с timer

Создаем файл с переменными окружения, файл, который будем парсить на наличие слова и сам скрипт который будет парсить
```
/etc/sysconfig/watchlog
/var/log/watchlog.log
/usr/bin/watchlog.sh
```
Создаем Юнит для Systemd
```
cat <<EOT>> /lib/systemd/system/watchlog.service
[Unit]
Description=My watchlog service
[Service]
Type=oneshot
EnvironmentFile=/etc/sysconfig/watchlog
ExecStart=/bin/bash /usr/bin/watchlog.sh \$WORD \$LOG

EOT
```
И таймер к нему 

```
cat <<EOT>> /lib/systemd/system/watchlog.timer
[Unit]
Description=Run watchlog script every 30 second
[Timer]
# Run every 30 second
OnUnitActiveSec=30
Unit=watchlog.service
[Install]
WantedBy=multi-user.target
EOT
```
Перечитываем конфигурацию юнитов и запускаем таймер и наш юнит.

```
systemctl daemon-reload
systemctl start watchlog.timer
systemctl start watchlog.service
```

# 2. Два httpd unit на разных портах

Устанавливаем и останавливаем Апач
```
yum install httpd -y
systemctl stop httpd
```
Переименовываем и добавляем параметр в конф. файл, что бы была возможность запускать юнит спараметром (@)
```
mv /usr/lib/systemd/system/httpd.service /usr/lib/systemd/system/httpd@.service
sed -i '/EnvironmentFile/s/httpd/httpd-%I/g' /usr/lib/systemd/system/httpd@.service
```
Создаем файлы с опциями для юнита, что бы грузить первую или вторую конфигурацию на выбор
```
echo "OPTIONS=-f conf/first.conf" > /etc/sysconfig/httpd-first
echo "OPTIONS=-f conf/second.conf" > /etc/sysconfig/httpd-second
```
Копируем оригинальный конфиг, переименовываем и меняем во втором конфиге порт и указываем новый PIDfile

```
cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/first.conf 
cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/second.conf
sed -i '/Listen 80/s/80/8080/g' /etc/httpd/conf/second.conf
echo "PidFile /var/run/httpd-second.pid" >> /etc/httpd/conf/second.conf

```
Перечитываем конфигурацию юнитов и запускаем  обе конфигурации

```
systemctl daemon-reload
systemctl start httpd@first
systemctl start httpd@second
```

# 3. Юнит с exit code 143
создаем скрипт с кодом завершения 143

```
cat <<EOT> /usr/bin/script3.sh
> /var/testscript
for i in {0..3}
do
DATE=\$(date +"%m-%d-%Y %k:%M:%S")
echo \$DATE >> /var/testscript
echo \$DATE
sleep 10
done
exit 143
EOT
```
Создаем для него юнит , перечитываем конфиги и стартуем этот юнит

```
cat <<EOT> /lib/systemd/system/exit143.service 
[Unit]
Description=Otus exit143 service.

[Service]
Type=simple
ExecStart=/bin/bash /usr/bin/script3.sh
SuccessExitStatus=143
Restart=on-success
MemoryLimit=100M
CPUQuota=5%
CPUAccounting=1
TasksMax=10

[Install]
WantedBy=multi-user.target
EOT

systemctl daemon-reload
systemctl start exit143.service
```

Юниту явно указано, какой код считать успешным завершением скрипта, поэтому была выбрана опция `Restart=on-success` 
что бы юнит каждый раз перезапускался после успешного выполнения с кодом 143.

Добавлен лимит по памяти, 
По CPU, 
Включен учет CPU, 
И установлено ограничение в 10 подзадач.(как я понял пункт TasksMax=)








